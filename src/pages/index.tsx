import Head from 'next/head';

import Heading from '@/components/heading/heading';
import ArticleList from '@/components/article/article-list';

export default function Index() {
    return (
        <>
            <Head>
                <title>The Hacker News</title>
                <meta name="description" content="Generated by create next app" />
                <meta name="viewport" content="width=device-width, initial-scale=1" />
                <link rel="icon" href="/favicon.ico" />
            </Head>

            <main className="is-widescreen">
                <Heading />
                <ArticleList />
            </main>
        </>
    );
}
