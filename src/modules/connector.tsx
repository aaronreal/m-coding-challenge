import axios from 'axios';

export type ArticleType = {
    title: string;
    url: string;
    by: string;
    score: number;
    time: number;
};

export type UserType = {
    id: string;
    karma: number;
};

export const getStories: () => Promise<ArticleType[]> = async (count = 10, offset = 0) => {
    // we use axios instead of fetch for a better streamlined API to make requests
    const response = await axios.get(
        `https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty&limitToFirst=${count}&orderBy="$key"`
    );

    const items = response.data;

    // fetch all requests in parallel
    const stories = (
        await axios.all(
            // using axios.all instead of Promise.all as per Axios recommendation
            items.map((item: number) => axios.get(`https://hacker-news.firebaseio.com/v0/item/${item}.json`))
        )
    ).map((item: any) => item.data);

    // sort stories
    stories.sort((a, b) => (a.score < b.score ? -1 : 1));

    return stories;
};

export const getUser: (username: string) => Promise<UserType> = async (username) => {
    const reponse = await axios.get(`https://hacker-news.firebaseio.com/v0/user/${username}.json?print=pretty`);

    return reponse.data;
};
