export default function Heading() {
    return (
        <div className="HeadingWrapper">
            <h1>The Hacker News</h1>
            <p>The latest hacker news snippets on the internet</p>
        </div>
    );
}
