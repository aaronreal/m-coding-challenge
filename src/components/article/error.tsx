import { faArrowsRotate } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export default function Error(props: { onClick: () => void }) {
    return (
        <div className="ErrorMessage">
            <div className="Message">Failed to fetch data, click refresh to try again</div>
            <FontAwesomeIcon onClick={() => props.onClick()} className="icon" icon={faArrowsRotate} />
        </div>
    );
}
