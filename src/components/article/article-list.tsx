import { useEffect, useState } from 'react';
import { faArrowsRotate } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import Loader from '@/components/loader/loader';
import { ArticleType, getStories } from '@/modules/connector';

import ArticleItem from './article-item';
import Error from './error';

export default function ArticleList() {
    const [isLoading, setLoading] = useState(true);
    const [isError, setError] = useState(false);
    const [items, setItems] = useState<ArticleType[]>([]);

    useEffect(() => {
        doFetchData();
    }, []);

    const doFetchData = async () => {
        setLoading(true);
        setError(false);

        try {
            const data = await getStories();
            setLoading(false);
            setItems(data);
        } catch (error) {
            setError(true);
        }
    };

    return (
        <div className="ArticleWrapper">
            {isLoading ? (
                <Loader />
            ) : (
                <>
                    {isError ? (
                        <Error
                            onClick={() => {
                                doFetchData();
                            }}
                        />
                    ) : (
                        <>
                            <div className="ArticleHeadingWrapper">
                                <h2>Latest Articles</h2>
                                <FontAwesomeIcon
                                    onClick={() => {
                                        doFetchData();
                                    }}
                                    className="icon"
                                    icon={faArrowsRotate}
                                />
                            </div>

                            <div className="Items">
                                {items.map((item, key) => (
                                    <ArticleItem key={key} {...item} />
                                ))}
                            </div>
                        </>
                    )}
                </>
            )}
        </div>
    );
}
