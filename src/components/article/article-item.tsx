import { useEffect, useState } from 'react';
import { faArrowUpRightFromSquare, faDove, faRotate } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { ArticleType, getUser, UserType } from '@/modules/connector';

export default function ArticleItem(props: ArticleType) {
    const [isUserLoading, setUserLoading] = useState(true);
    const [user, setUser] = useState<UserType | null>(null);

    const date = new Date(props.time * 1000);

    useEffect(() => {
        // offload async function into it's own closure as per useEffect is not
        // recommended to be asynced directly
        const init = async () => {
            try {
                const user = await getUser(props.by);
                setUser(user);
            } catch (error) {
                setUser(null);
            }

            setUserLoading(false);
        };

        init();
    }, []);

    return (
        <div className="ArticleItem">
            <img src="https://placehold.co/200x100" alt="Story Image" />

            <div className="Details">
                <div className="TitleWrapper">
                    <a href={props.url} target="_blank">
                        <div className="Title">
                            <h3>{props.title}</h3>
                            <FontAwesomeIcon className="icon" icon={faArrowUpRightFromSquare} />
                        </div>
                    </a>
                </div>

                <p className="Timestamp">{`${date.toLocaleDateString()} ${date.getHours()}:${date.getMinutes()}`}</p>

                <p className="ScoreInfo">Score: {props.score}</p>

                <p className="AuthorInfo">
                    <span>By</span>
                    <span className="Author">{` ${props.by} `}</span>
                    <span>
                        (Karma:{' '}
                        {isUserLoading ? (
                            <FontAwesomeIcon className="icon animate-spin" icon={faRotate} />
                        ) : (
                            <>{user ? <span>{user.karma}</span> : <span>-</span>}</>
                        )}
                        )
                    </span>
                </p>
            </div>
        </div>
    );
}
