# The Hacker News

### Libraries Used
- NextJS 13
- SASS

### Prerequisite

- Node 16+
- Yarn

## Usage

Install the dependencies:

```bash
yarn install # or npm install
```

Then run:

```bash
yarn dev # or npm run dev
```

Open [http://localhost:3000](http://localhost:3000) on your browser